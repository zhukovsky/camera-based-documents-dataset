#Camera-Based Documents Dataset

Here is repository to maintain dataset for comparing algorithms for camera-based documents detection.

----
Dataset structure is rather simple:

+ directory *images* contains images, compressed with jpg, named %05d.jpg
+ directory *annotations* contains text files with annotations, named %05d.jpg.txt (coincident
with image name). Every annotation contains 8 double-precision numbers for 4 points on image - document quadrangle points, clockwise from top left point. Annotation has the next format:
```
x0 y0
x1 y1
x2 y2
x3 y3
```